import { Document, Schema } from 'mongoose';

export const userModelSchema = new Schema(
  {
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
  },
  { collection: 'users' },
);

export interface IUserModel extends Document {
  email: string;
  password: string;
}

