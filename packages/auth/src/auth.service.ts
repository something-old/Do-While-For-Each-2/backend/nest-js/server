import { Model } from 'mongoose';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { IUserModel } from './models/user';

@Injectable()
export class AuthService {

  constructor(@InjectModel('userModelInjectToken') private readonly userCollection: Model<IUserModel>) {
  }

  findUser = async (email: string): Promise<any> =>
    this.userCollection
      .findOne({ email })
      .then(candidate => {
          if (candidate)
            throw new HttpException('Email present in database', HttpStatus.BAD_REQUEST);
          else
            return 'user none';
        },
      );

  // .catch(error => log.errorReThrowOrThrow(error, 'Check exist user error'))


  getHello(): string {
    return 'Hello World 123!';
  }
}
